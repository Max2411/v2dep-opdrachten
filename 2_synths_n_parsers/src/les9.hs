---- Functies voor State (niet aankomen)
--
--import Control.Monad (ap)
--import Control.Applicative (Applicative(..), liftA)
--
--newtype State s a = State {runState :: s -> (a, s)}
--
--instance Functor (State s) where
--    fmap = liftA
--
--instance Applicative (State s) where
--    {-# INLINE pure #-}
--    pure x = State $ \ s -> (x, s)
--    (<*>) = ap
--
--instance Monad (State s) where
--    {-# INLINE return #-}
--    {-# INLINE (>>=) #-}
--    return = pure
--    m >>= k = State $ \ s -> case runState m s of
--        (x, s') -> runState (k x) s'
--
--get :: State s s
--get = State $ \s -> (s, s)
--
--put :: s -> State s ()
--put newState = State $ \_ -> ((), newState)
--
--modify :: (s -> s) -> State s ()
--modify f = do s <- get
--              put $ f s
--
---- runState :: State s a -> s -> (a, s) -- Van de constructor
--
--evalState :: State s a -> s -> a
--evalState = fmap fst . runState      -- Laatste resultaat
--
--execState :: State s a -> s -> s
--execState = fmap snd . runState      -- Eindstate
--
---- Functies voor ons programma
--
--type Stand = (String, Integer, Integer, String)
--
--tussenstand :: Stand
--tussenstand = ("Emmen", 3, 4, "VVV-Venlo")
--
--toonStand :: Stand -> String  -- Helperfunctie, om de stand te laten zien
--toonStand (tt, ts, us, ut) = concat [tt, "  ", show ts, "-", show us, "  ", ut]
--
--doelpunt' :: Bool -> State Stand String
--doelpunt' thuisofuit = State (\(tt, ts, us, ut) ->
--  let ts' = if thuisofuit then ts+1 else ts
--      us' = if thuisofuit then us else us+1
--      leider = if ts' > us' then tt else (if ts' < us' then ut else "Gelijkspel")
--  in (leider, (tt, ts', us', ut)))
--
--
---- doelpunt'' :: ???
--doelpunt'' = undefined
--
--thuisDoelpunt' :: State Stand String
--thuisDoelpunt' = doelpunt'' True
--
--uitDoelpunt' :: State Stand String
--uitDoelpunt' = doelpunt'' False
--
--fcg_psv' :: State Stand String
--fcg_psv' = do uitDoelpunt'
--              thuisDoelpunt'
--              uitDoelpunt'
--              uitDoelpunt'
--
--hoogOp :: Int -> Int
--hoogOp a = succ a
--
--hoogOp' :: Int -> Int
--hoogOp' = \a -> succ a
--
--data VerpakteFunctie = Doos (Int -> Int)
--
--hoogOp'' :: VerpakteFunctie
--hoogOp'' = Doos $ \a -> succ a
---- h = Doos (\a -> succ a)
--
--pakUitEnPasToeOp :: VerpakteFunctie -> Int -> Int
--pakUitEnPasToeOp (Doos functie) waarde = functie waarde
--
--combineerVerpakteFuncties :: VerpakteFunctie -> VerpakteFunctie -> VerpakteFunctie
--combineerVerpakteFuncties (Doos f1) (Doos f2) = Doos $ f1 . f2
---- f1 . f2 = \x -> f1 (f2 x)
--
--{-
-- - type Hz = Float
-- - type Seconds = Float
-- - type Pulse = [Float]
-- - data Instrument = Instrument (Float -> Float -> [Float])
-- -
-- - geenInstrument hz seconde = [hz, seconde]
-- -}
--
----i :: VerpakteFunctie
----i a = undefined


import Control.Monad.Trans.State
import Control.Monad
import Control.Applicative

type Parser = (StateT String Maybe)

pChar :: Parser Char
pChar = do text <- get
           if length text > 0
             then modify tail >> return (head text)
             else mzero

parseOn = runStateT

pTwoChars :: Parser [Char]
pTwoChars = do firstChar <- pChar
               secondChar <- pChar
               return [firstChar, secondChar]

pString :: Parser String
pString = do firstChar <- pChar
             rest <- pString <|> pEmpty
             return $ firstChar : rest

pEmpty :: Parser String
pEmpty = do text <- get
            if null text
              then return ""
              else mzero

pSpecificChar :: Char -> Parser Char
pSpecificChar sought = do parsed <- pChar
                          if parsed == sought
                            then return parsed
                            else mzero


-- <|> combineert twee parsers door beide te proberen
