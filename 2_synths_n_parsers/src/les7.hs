--data Person = Person { name   :: String        -- Record syntax
--                     , father :: Maybe Person  -- maakt automatisch functies:
--                     , mother :: Maybe Person  -- father :: Person -> Maybe Person
--                     } deriving (Show)                        -- zelfde voor name, mother
--
--rickard = Person "Rickard" Nothing Nothing
--ned = Person "Ned" (Just rickard) Nothing
--robb = Person "Robb" (Just ned) Nothing
--
--grandfather :: Person -> Maybe Person
--grandfather p = father p >>= father
--
--countParents :: Person -> Int
--countParents p = undefined

main :: IO ()
main = do putStrLn "Hello,im console \nwho are you"
          name <- getLine
          putStrLn ("<><><><><><><>\n"++name++"!\n<><><><><><><><><><>")




