{-|
    Module      : Parsers
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat parsers en functies om deze te combineren. We gebruiken simpele parsers om meer complexe parsers op te bouwen.
-}

module Parsers (Parser, parse, pCharSet, pComplementCharSet, pString, pOptional, pRepeat, pNumber, pOctave, pHeader, parse) where

import Types (Octave, Beats, Duration(..), Note(..), Tone(..))
import Control.Applicative ((<|>))
import Control.Monad.Trans.State (StateT(..), evalStateT, put, get, modify)
import Data.Maybe (isJust, fromMaybe)
import Data.Char (toUpper)
import Control.Monad(mzero, mplus)
import Data.List (uncons)


type Parser = (StateT String Maybe)
type CharSet = [Char]

-- test: runStateT (pCharSet "abc") "cde"
pCharSet :: CharSet -> Parser Char
pCharSet cs = do input <- uncons <$> get
                 case input of
                   Just (h, t) -> if h `elem` cs then put t >> return h else mzero
                   Nothing -> mzero

-- TODO Schrijf een `Parser` `pComplementCharSet` die een lijst van karakters meekrijgt (ook wel een `String`)
--  en het eerste karakter parset wanneer dit niet in de meegegeven set karakters zit.
{-| pComplementCharSet pakt een lijst met characters die die gaat zoeken. Deze functie gaat dan deze characters
zoeken in de string die is gehaald uit de state. Deze string uit de state wordt opgesplits in een head en tail.
Als de head in de string zit die wordt meegegeven aan pComplement wordt nothing mee gegeven. Als dit niet zo is wordt
de head terug gegeven en wordt de tail weer terug gestopt in de state. Dus als de head van de state niet in
de meegegeven string zit wordt de head terug gegeven.-}
pComplementCharSet :: CharSet -> Parser Char
pComplementCharSet cs = do input <- uncons <$> get
                           case input of
                             Just (h, t) -> if h `elem` cs then mzero else put t >> return h
                             Nothing -> mzero

-- TODO Schrijf een `Parser` `pString` die een gegegeven `String` probeert te parsen. Gebruik hiervoor `do`
--  notatie; parse een enkele letter met `pCharSet` en parse de rest recursief met `pString`; combineer beide
--  waarden weer voordat je deze `return`t. Vergeet niet een geval voor een lege `String`  te schrijven.
{-| Deze functie maakt een lijst van de meegegeven string. De head van de lijst wordt dan toegepast op pcharset
en dit wordt gestopt in de variable firstS. Daarna wordt de tail gebruikt voor de recursie om op de rest van de string
te checken. dit wordt dan achteraf in restS gestopt. Deze recursie gaat door tot dat het item is gevonden
of tot dat er een lege string is als variable voor wat je zoekt. Als het item wordt gevonden komt dit in rest zonder
de head van het woord, dit wordt daarna gereturned door firstS samen te voegen met restS. Als er een lege string over
is betekened het dat het item niet is gevonden en dus wordt er mzero terug gegeven.-}
pString :: String -> Parser String
pString "" = return mzero
pString (s:xs) = do firstS <- pCharSet [s]
                    restS  <- pString xs
                    return $ [firstS] ++ restS


-- TODO Schrijf een `Parser` `pOpttional` die gegeven een `Parser` optioneel maakt. Als de `Parser` slaagt
--  wordt een `Just` value teruggegeven, zo niet wordt `Nothing` ge`return`ed. Je kunt hiervoor gebruik
--  maken van `mplus` uit `Control.Monad`.
{-| Deze functie krijgt een ingepakt parsers mee met p. Deze wordt uitgepakt met de fmap(<$>) functor om deze parsers
 toe tepassen en als de fmap functor het niet doet zal er nothing gereturned worden om alle mogelijkheden aftebaken
 tegen errors.-}
pOptional :: Parser a -> Parser (Maybe a)
pOptional p = Just <$> p <|> return Nothing

pRepeatSepBy :: Parser a -> Parser b -> Parser [b]
pRepeatSepBy sep p = (:) <$> p <*> mplus (sep *> pRepeatSepBy sep p) (return [])

-- De empty parser, deze parset niets en geeft `()` terug.
pEmpty :: Parser ()
pEmpty = return ()

-- TODO Combineer `pRepeatSepBy` en `pEmpty` tot een `Parser` `pRepeat` die een enkele `Parser` herhaalt.
{-| Deze functie past de pRepeatSepBy toe op 1 parser door als tweede parsers een lege parsers
meet te geven.-}
pRepeat :: Parser a -> Parser [a]
pRepeat p = pRepeatSepBy pEmpty p

numbers :: CharSet
numbers = "0123456789"

-- TODO Combineer `pRepeat` en `pCharSet` tot een `Parser` die een getal als `String` leest, en roep hier `read`
--  op aan om een `Int` terug te geven.
{-| Deze functie combineert de pRepeat met de pCharSet die een getal als `String` leest door de de numbers op
 pCharSet toetepassen en de pCharSet op de pRepeat te doen. Daarna worden de numbers van string als een int gelezen
 door de read functie met fmap toe te passen op de uitkomst van het combineren van pRepeat en pCharSet-}
pNumber :: Parser Int
pNumber = read <$> (pRepeat $ pCharSet numbers)

pTone :: Parser Tone
pTone = do tone <- tRead . toUpper <$> pCharSet "abcdefg"
           sharp <- pOptional (pCharSet "#")
           if isJust sharp && tone `elem` [C,D,F,G,A]
             then return (succ tone)
             else return tone
  where tRead 'C' = C
        tRead 'D' = D
        tRead 'E' = E
        tRead 'F' = F
        tRead 'G' = G
        tRead 'A' = A
        tRead 'B' = B
        tRead _   = error "Invalid note"

-- TODO Schrijf een `Parser` `pOctave`. Je kunt `toEnum` gebruiken om een `Int` naar een `Octave` te casten.
{-| Deze functie past met de fmap functor toEnum toe op de uitkomst van pNumber.-}
pOctave :: Parser Octave
pOctave = toEnum <$> pNumber

pDuration :: Parser Duration
pDuration = do number <- pNumber
               case number of
                 1 -> return Full
                 2 -> return Half
                 4 -> return Quarter
                 8 -> return Eighth
                 16 -> return Sixteenth
                 32 -> return Thirtysecond
                 _ -> mzero

pPause :: Duration -> Parser Note
pPause d = do duration <- fromMaybe d <$> pOptional pDuration
              _ <- pCharSet "pP"
              return $ Pause duration

pNote :: Duration -> Octave -> Parser Note
pNote d o = do duration <- fromMaybe d <$> pOptional pDuration
               tone <- pTone
               dot <- pOptional (pCharSet ".")
               octave <- fromMaybe o <$> pOptional pOctave
               return $ Note tone octave (if isJust dot then Dotted duration else duration)

pComma :: Parser ()
pComma = () <$ do _ <- pCharSet ","
                  pOptional (pCharSet " ")

-- TODO Pas deze `Parser` aan zodat de de titel uit de RTTL string wordt gehaald en in de plaats van PLACEHOLDER
-- wordt teruggegeven.
pHeader :: Parser (String, Duration, Octave, Beats)
pHeader = do title <- pRepeat (pComplementCharSet ":")
             _ <- pCharSet ":"
             _ <- pOptional (pCharSet " ")
             _ <- pString "d="
             duration <- pDuration
             _ <- pComma
             _ <- pString "o="
             octave <- pOctave
             _ <- pComma
             _ <- pString "b="
             bpm <- fromIntegral <$> pNumber
             _ <- pCharSet ":"
             _ <- pOptional (pCharSet " ")
             return (title, duration, octave, bpm)

pSeparator :: Parser ()
pSeparator = () <$ foldl1 mplus [pString " ", pString ", ", pString ","]

pRTTL :: Parser (String, [Note], Beats)
pRTTL = do (t, d, o, b) <- pHeader
           notes <- pRepeatSepBy pSeparator $ mplus (pNote d o) (pPause d)
           return (t, notes, b)

-- TODO Schrijf een functie `parse` die `pRTTL` aanroept. Bedenk hierbij dat een
--  `Parser` eigenlijk niet meer is dan een `StateT` met een `Maybe` erin.
{-| Voert de pRTTL functie uit via de evalStateT op s zodat een string wordt omgezet in een sound file-}
parse :: String -> Maybe (String, [Note], Beats)
parse s = evalStateT pRTTL s
