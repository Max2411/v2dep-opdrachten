{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat alle type-declaraties, instanties en type-gerelateerde functies, zodat we deze makkelijk in meerdere modules kunnen importeren.
-}

{-# LANGUAGE TypeApplications #-}

module Types ( Beats, Hz, Samples, Seconds, Semitones, Track, Ringtone
             , Tone(..), Octave(..), Duration(..), Note(..)
             , Sound, floatSound, intSound, (<+>), asFloatSound, asIntSound, getAsFloats, getAsInts
             , Instrument, instrument, Modifier, modifier, modifyInstrument, arrange
             ) where

import Data.Int (Int32)
import Util

type Pulse = [Float]
type Seconds = Float
type Samples = Float
type Hz = Float
type Semitones = Float
type Beats = Float
type Ringtone = String

data Tone = C | CSharp | D | DSharp | E | F | FSharp | G | GSharp | A | ASharp | B deriving (Enum, Eq, Show)
data Octave = Zero | One |  Two | Three | Four | Five | Six | Seven | Eight deriving (Enum, Eq, Show)
data Duration = Full | Half | Quarter | Eighth | Sixteenth | Thirtysecond | Dotted Duration deriving (Eq, Show)
data Note = Pause Duration | Note Tone Octave Duration deriving (Eq, Show)

data Sound = IntFrames [Int32] | FloatFrames [Float]
  deriving (Eq, Show)

floatSound :: [Float] -> Sound
floatSound = FloatFrames

intSound :: [Int32] -> Sound
intSound = IntFrames

-- TODO Maak instances voor `Sound` voor `Semigroup` en `Monoid`. De monoid-operatie staat in dit geval voor het
--  sequentieel (achter elkaar) combineren van audiofragmenten. Het is van belang dat `IntFrames` alleen met `IntFrames`
--  worden gecombineerd, en dito voor `FloatFrames`. Bij twee verschillende gevallen moet je beiden naar hetzelfde
--  formaat converteren, idealiter `FloatFrames`. Wat is een leeg audiofragment in deze context?

instance Semigroup Sound where
  (IntFrames x) <> (IntFrames y) = IntFrames (x ++ y)
  (FloatFrames x) <> (IntFrames y) = FloatFrames (x ++ map (fromIntegral) y)
  (IntFrames x) <> (FloatFrames y) = FloatFrames (map (fromIntegral) x ++ y)
  (FloatFrames x) <> (FloatFrames y) = FloatFrames (x ++ y)

instance Monoid Sound where
  mempty = IntFrames []

-- TODO Maak een operator `(<+>)` die twee `Sound`s  tot een enkel `Sound` combineert door de geluiden tegelijk
--  af te spreken. Dit mag door de frames als in een `zipWith (+)` samen te voegen, maar in dit geval wordt niet
--  de kortste maar de langste lijst aangehouden (in dat geval wordt enkel het aanwezige geluid weergegeven). Je
--  hoeft deze operator alleen op `FloatFrames` te matchen, de laatste regel converteert alles hierheen als een of
--    beide argumenten in `IntFrames` staan.
{-| de <+> functie voegt twee functies samen. Dit wordt gedaan door de kleine functie bij de grote te doen zodat je
de sound met de grootste lengte overhoud. Door middel van de zipWithL en de zipWithR functie wordt dit gedaan.-}
(<+>) :: Sound -> Sound -> Sound
(FloatFrames x) <+> (FloatFrames y) | length x >= length y = FloatFrames (zipWithL (+) x y)
                                    | length x < length y = FloatFrames (zipWithR (+) x y)
x <+> y = asFloatSound x <+> asFloatSound y

asFloatSound :: Sound -> Sound
asFloatSound (IntFrames fs) = floatSound $ map ( (/ fromIntegral (div (maxBound @Int32 ) 2 )) . fromIntegral ) fs
asFloatSound fframe = fframe

-- TODO Maak een functie `asIntSOund` die als inverse van `asFloatSound` fungeert.
{-| Deze functie maakt van een float frame een intframe door het tegenovergestelde te doen van de asFloatSound functie.
Dus deze functie doet het grootste getal ook gedeeld door twee. Daarna zal dit getal in de map functie worden gedaan met een
keer teken en samen met de round functie. Hierdoor worden alle getallen in de lijst vermenigvuldigd en dan afgerond
zodat dit ints kunnen worden. Als dit is gedaan zal de functie een intFrame worden door de intSound functie. Als
het al een intframe was en dus geen floatframe zal dit gewoon worden terug gegeven met deze functie-}
asIntSound :: Sound -> Sound
asIntSound (FloatFrames fs) = intSound $ map ( round . (* fromIntegral (div (maxBound @Int32 ) 2 )) ) fs
asIntSound fframe = fframe

getAsFloats :: Sound -> [Float]
getAsFloats sound = case asFloatSound sound of
  (FloatFrames ffs) -> ffs
  _ -> error "asFloatSound did not return FloatFrames"

getAsInts :: Sound -> [Int32]
getAsInts sound = case asIntSound sound of
  (IntFrames ifs) -> ifs
  _ -> error "asIntSound did not return IntFrames"

type Track = (Instrument, [Note])

newtype Instrument = Instrument (Hz -> Seconds -> Pulse)

instrument :: (Hz -> Seconds -> Pulse) -> Instrument
instrument = Instrument

newtype Modifier = Modifier (Pulse -> Pulse)

modifier :: (Pulse -> Pulse) -> Modifier
modifier = Modifier

instance Semigroup Modifier where
  (Modifier m1) <> (Modifier m2) = Modifier $ m1 . m2

-- TODO Maak een functie `modifyInstrument) die een `Modifier` met een `Instrument` combineert.
--  Gebruik een lambda om een nieuw instrument terug te geven, waarbij de functie in de modifier
--  met de functie in het instrument gecomposed wordt.
-- modinst.      :: (Hz -> Seconds ->  Pulse) -> (pulse -> pulse) -> (Hz -> Seconds ->  Pulse)
-- (hz -> sec -> (pulse -pulse))
--  |instrument|     modifier
{-| Deze functie modified een instrument. Dit doet die doro de instrument functie toe tepassen met lambda waarbij
de hz en sec worden gegeven. Deze hz en sec zal worden toegepast op het meegegeven instrument (ins). Hieruit komt een
pulse die dan kan gemodified worden door de modifier functie die is meegegeven, modi. Als dit dan is gedaan wordt de
uitkomst weer terug gezet in een instrument-}
modifyInstrument :: Instrument ->                 Modifier ->         Instrument
modifyInstrument (Instrument ins) (Modifier modi) = Instrument (\hz sec ->modi(ins hz sec) )

-- TODO Maak een functie `arrange` die de functie in het meegegeven `Instrument` toepast op de
--  frequentie en duur. Het resultaat wordt als `Sound` verpakt.
{-| Deze functie maakt een sound door de meegegeven instrument uit tevoeren met de meegegevn hz en seconden. Deze uitkomst
wordt hierna omgezet naar een floatFrame om er een sound van te maken.-}
arrange :: Instrument -> Hz -> Seconds -> Sound
arrange (Instrument ins) h s= FloatFrames(ins h s)
