import Prelude hiding (map, foldr, foldr1, foldl, foldl1, zipWith)

-- map
map :: (a->b)->[a] -> [b]
map f [] = []
map  f (x:xs) = (f x) : map f xs

--allesNegate = map negate

allesPlus1 x = map (+1) x

-- zipWith
--zipWith :: [a] -> [b] -> [(a, b)]
zipWith :: (a->b->c)->[a] -> [b] -> [c]
zipWith f [] _ = []
zipWith f _ [] = []
zipWith f (x:xs) (y:ys) = x `f` y :(zipWith f xs ys)

-- foldr1
foldr1 :: (a->a->a)->[a] -> a
foldr1 f [x] = x
foldr1 f (x:xs) = x `f` (foldr1 f xs)

-- foldr
foldr :: (a->b->b)-> b -> [a] -> b
foldr f b [x] = x
foldr f (x:xs) = x `f` (foldr f b xs)


--(<>) :: s -> s -> s
--(<>) :: [a] -> [a] -> [a]
--(x <> y) <> z == x <> (y <> z)

test 3 = undefined
test x | x > 0 = undefined
       | x <= 0 = undefined