module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.
{- Deze functie pakt het hoofd van de lijst en telt deze op bij het volgende hoofd door de functie opnieuw uit te voeren
op de staart. Dit doet die tot het laatste item, de lege lijsts. Een lege lijst heeft geen int dus vooraf is aangegeven
dat een lege lijst 0 is zodat er op het eind niks bij komt.-}
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs) = x + ex1 xs

-- Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
{- Deze functie telt 1 op bij het hoofd van de lijst en voert daarna de functie opnieuw aan op de staart. Dit doet die
tot het laatste item, de lege lijst. Een lege lijst kan je niks bij opgetellen en dus is er vooraf aangegeven dat een
lege lijst een lege lijst blijft.-}
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x + 1 : ex2 xs

-- Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
{- Deze functie doet het hoofd van de lijst keer -1 zodat de waarde negatief wordt en voert daarna de functie opnieuw
aan op de staart. Dit doet die tot het laatste item, de lege lijst. Een lege lijst kan je niet vermenigvuldigen en dus
is er vooraf aangegeven dat een lege lijst een lege lijst blijft.-}
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1) : ex3 xs

-- Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
-- Deze lijst comibineert twee lijsten doorze achter elkaar te plakken waardoor er 1 lijst over blijft.
ex4 :: [Int] -> [Int] -> [Int]
ex4 lst1 lst2 = lst1 ++ lst2

-- Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
{- Deze functie pakt de hoofden van twee lijsten met gelijke lengthe en telt deze bij elkaar op en voert daarna dit
opnieuw uit op de staarten van de twee lijst. Dit gaat door tot de laatste items van de lijsten, de lege lege lijst.
Deze kun je niet optellen dus is vooraf aangegeven dat bij twee lijsten worden verandert naar 1 lijst.-}
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 (x:xs) (y:ys) = x + y : ex5 xs ys

-- Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
{- {- Deze functie pakt de hoofden van twee lijsten met gelijke lengthe en vermenigvuldigd deze en voert daarna dit
   opnieuw uit op de staarten van de twee lijst. Dit gaat door tot de laatste items van de lijsten, de lege lege lijst.
   Deze kun je niet vermenigvuldigen dus is vooraf aangegeven dat bij twee lijsten worden verandert naar 1 lijst.-}-}
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 (x:xs) (y:ys) = x*y : ex6 xs ys

-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
{- Deze functie neemt twee lijst met gelijke lengthe en voert dan eerst fucntie ex6 uit om de twee met elkaar
te vermenigvuldigen. Als dit is gedaan wordt functie ex1 uitgevoerd om de uitkomsten van deze vermenigvuldigingen bij
elkaar op te tellen tot 1 getal.-}
ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1 (ex6 x y)
