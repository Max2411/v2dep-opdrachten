heen :: a -> (a,())
heen x = (x,())

terug :: (a,()) -> a
terug (x,()) = x


--heen1 :: Either a a -> (Bool,a)
--heen1 (Right a) = (True,a)
--heen1 (Left a) = (False,a)
--
--terug1 :: (bool,a) -> Either a a
--terug1 (True,a) = Right a
--terug1 (False,a) = Left a


heen_dist :: (a,Either b c) -> Either (a, b) (a, c)
heen_dist (a, Left b) = Left (a, b)
heen_dist (a, Right c) = Right (a, c)

terug_dist :: Either (a,b) (a,c) -> (a,Either b c)
terug_dist (Left (a,b)) = (a, Left b)
terug_dist (Right (a,c)) = (a, Right c)
--terug_dist (a,b) (a,c) = (a,(b,c))

data Huisdier = Kat { knaam :: String, kkleur :: String} | Hond {hnaam :: String, hkleur :: String } deriving Show
data Huisdier2 = Kat | Hond deriving Show
data Dier = Dier { soort :: Huisdier2,
                   dnaam :: String,
                   dkleur :: String } deriving Show
data Persoon = Persoon { naam :: String,
                         schoenmaat :: Int,
                         lenghte :: Int,
                         heeftBril :: Bool,
                         favorKleur :: String,
                         huisdier :: [Huisdier] } deriving Show
--data Persoon2 = Persoon2 { naam2 :: String,
--                         schoenmaat2 :: Int,
--                         lenghte2 :: Int,
--                         heeftBril2 :: Bool,
--                         favorKleur2 :: String,
--                         huisdier2 :: [] } deriving Show

jaap = Persoon "Jaap" 45 190 True "blauw" [Kat "gerit" "Roze", Kat "donald" "wit"]
--dd = Persoon "dd" 39 201 False "geel" [Kat,Kat,Hond,Kat,Hond]
--jaap2 = Persoon2 "Jaap" 45 190 True "blauw" [[Kat,"Gerit",5,"zwart"],[Kat,"Rana",10,"blond"]]

voornaam :: Persoon -> String
voornaam (Persoon naam _ _ _ _ _) = naam

bril :: Persoon -> Bool
bril (Persoon _ _ _ bril _ _) = bril

dier :: Persoon -> [Huisdier]
dier (Persoon _ _ _ _ _ huisdier) = huisdier

huisdiernaam :: Huisdier -> String
huisdiernaam (Kat knaam _ ) = knaam
huisdiernaam (Hond knaam _ ) = knaam

diernaam :: [Huisdier] -> [String]
diernaam [] = []
diernaam (x:xs) = huisdiernaam x: diernaam xs

diernamen :: Persoon -> [String]
diernamen (Persoon _ _ _ _ _ huisdieren) = diernaam huisdieren